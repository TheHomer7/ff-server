﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ServerEngine.IO.Configs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ServerEngine.IO
{
    public static class ConfigController
    {
        private static string NameFolderConfigs = "Configs";

        private static string PathBase = AppDomain.CurrentDomain.BaseDirectory;

        private static string ConfigsFolderPath = Path.Combine(PathBase, NameFolderConfigs);

        private static string DefaultExtension = ".cfg";

        public static SystemConfig g_SystemConfig;

        public static void InitConfigs()
        {
            var path = Path.Combine(ConfigsFolderPath, "system" + DefaultExtension);

            if (!Directory.Exists(ConfigsFolderPath))
                Directory.CreateDirectory(ConfigsFolderPath);

            if (!File.Exists(path))
            {
                g_SystemConfig = new SystemConfig();
                CreateConfig<SystemConfig>("system", g_SystemConfig);
            }
            else
            {
                g_SystemConfig = LoadConfig<SystemConfig>("system");
            }

        }



        public static string CreateConfig<T>(string name, IConfig settings)
        {
            var config = (T)settings;

            if (File.Exists(Path.Combine(ConfigsFolderPath, name + DefaultExtension)))
            {
                return $"false:Такой настроечный файл уже существует {name}";
            }
            string path = Path.Combine(ConfigsFolderPath, name + DefaultExtension);

            var config_file = File.Create(path);

            using (FileStream fstream = config_file)
            {
                var json = JsonConvert.SerializeObject(config, Formatting.Indented);
                byte[] array = System.Text.Encoding.UTF8.GetBytes(json);
                fstream.Write(array, 0, array.Length);
                return $"true:Файл настройки {name} успешно создан. ";
            }
        }

        public static T LoadConfig<T>(string name)
        {
            var path = Path.Combine(ConfigsFolderPath, name + DefaultExtension);

            if (!File.Exists(path))
            {
                return (T)Activator.CreateInstance(typeof(T));
            }

            var file = File.Open(path, FileMode.Open);

            using (FileStream fstream = file)
            {
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                string json = System.Text.Encoding.Default.GetString(array);

                var config = JsonConvert.DeserializeObject<T>(json);
                return config;
            }
        }


    }
}
