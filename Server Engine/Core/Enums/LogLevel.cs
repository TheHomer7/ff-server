﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerEngine.Core.Enums
{
    public enum LogLevel
    {
        Info = 0,
        Debug,
        DebugCore,
        System       
    }
}
