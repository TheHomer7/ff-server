﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerEngine.Core.Interfaces
{
    public interface IStarted
    {
        public abstract void Start();
    }
}
