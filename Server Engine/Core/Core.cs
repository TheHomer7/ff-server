﻿using NetcodeIO.NET;
using ServerEngine.Core.Attributes;
using ServerEngine.Core.Enums;
using ServerEngine.Core.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace ServerEngine.Core
{
    public static class Core
    {
        public static ConcurrentDictionary<string, RemoteClient> remoteClients = new ConcurrentDictionary<string,RemoteClient>();

        public static LogLevel logLevel = LogLevel.Debug;

        private static ObjectPool<object> instances = new ObjectPool<object>();

        private static RpcPool rpc = new RpcPool();

        private static List<Assembly> assemblies = new List<Assembly>();

        public static Assembly ServerAssembly;

        public static double _time;
        public static double _deltatime;

        public static void Initialize()
        {
            assemblies = AppDomain.CurrentDomain.GetAssemblies().ToList();

            try
            {
                ServerAssembly = Assembly.LoadFrom(@"ServerAssembly.dll");
                assemblies.Add(ServerAssembly);
            }

            catch (Exception ex)
            {
                Program.StopServer(ex.ToString());
            }

            _time = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            FindRpcs();
            if (logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine("Start initialization CORE server");
            }

            ServerBehavior.OnConnectClient += ConnectClient;
            ServerBehavior.OnDisconnectClient += DisconnectClient;

            Program.core.OnServerTick += Tick;

        }

        public static Type GetType(string name)
        {
            var types = assemblies
               .SelectMany(x => x.GetTypes())
               .ToList(); // returns only methods that have the InvokeAttribute

            var type = types.Where(x => x.FullName == name).FirstOrDefault();

            if (type == null)
            {
                type = Type.GetType(name, false);
            }

            return type;
        }

        private static void ConnectClient (RemoteClient client)
        {
            remoteClients.TryAdd(client.ClientID.ToString(), client);
            Console.WriteLine($"Client {client.ClientID} connected ");
        }

        private static void DisconnectClient(RemoteClient client)
        {
            remoteClients.TryRemove(client.ClientID.ToString(), out _);
            Console.WriteLine($"Client {client.ClientID} disconnected ");
        }

        public static void ExecuteAssemblyMain()
        {
            var runtime = ServerAssembly.CreateInstance("ServerAssembly.Runtime");
            instances.PutObject(runtime);
            Console.WriteLine("Executing assembly main method");
        }

        private static void ExecuteStartMethod(object invoker)
        {
            try
            {
                if (invoker is IStarted inv)
                    inv.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }


        }

        private static void Tick(double time)
        {
            try
            {
                _deltatime = time -_time;
                _time = time;

                Console.Title = CalculateFrameRate().ToString();

                var objects = instances.GetAllObject().ToList();

                if (objects.Count < 1)
                {
                    return;
                }

                Parallel.ForEach(objects, x => { if (x is ITicked item) item.Update(); });

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static int lastTick;
        private static int lastFrameRate;
        private static int frameRate;

        private static int CalculateFrameRate()
        {

            if (System.Environment.TickCount - lastTick >= 1000)
            {
                lastFrameRate = frameRate;
                frameRate = 0;
                lastTick = System.Environment.TickCount;
            }
            frameRate++;
            return lastFrameRate;
        }

        public static void AddToPool(object item)
        {
            instances.PutObject(item);
            ExecuteStartMethod(item);
            if (logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine($"Object poll put new object of type: {item.GetType().ToString()}");
            }
        }

        public static void RemoveFromPool(object item)
        {
            instances.RemoveObject(item);
            if (logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine($"Object poll put new object of type: {item.GetType().ToString()}");
            }
        }

        public static void FindRpcs ()
        {
            if (logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine("Start search RPC methods in server");
            }

            var rpccomand = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => x.IsClass)
                .Where(x => x.BaseType == typeof(ServerBehavior))
                .SelectMany(x => x.GetMethods())
                .Where(x => x.GetCustomAttributes(typeof(RPCAttribute), false).FirstOrDefault() != null).ToList(); // returns only methods that have the InvokeAttribute

            foreach (var item in rpccomand)
            {
                var parametrs = item.GetParameters();

                string parametrs_string = "";

                foreach (var param in parametrs)
                {
                    parametrs_string += $"{param.ParameterType.FullName};";
                }

                string name = $"{item.Name}::{parametrs_string}";

                rpc.PutObject(name, item);
                if (logLevel >= LogLevel.System)
                {
                    Console.WriteLine($"RPCPOOL ADD NEW RPC METHOD : {name}");
                }
            }

            if (logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine($"Ending search  RPC methods in server. Found {rpc.Count}");
            }

        }

        public static string ExecuteRPC(string name, object[] ps)
        {
            if (logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine($"Execute RPC {name}");
            }

            try
            {
                var method = rpc.ExecuteObject(name);

                if (method == null)
                {
                    return $"false: rpc команды : {name}, нету в системе";
                }

                var objects = instances.GetAllObject().Where(x => x.GetType() == method.DeclaringType).ToList();

                if (objects.Count < 1)
                {
                    return "";
                }

                Parallel.ForEach(objects, x => method.Invoke(x, ps));

                return "";
            }
            catch (Exception ex)
            {
                return "";
            }
        }

    }
}
