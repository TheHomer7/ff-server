﻿using NetcodeIO.NET;
using System;
using System.Collections.Generic;
using System.Text;
using ServerEngine.Core;
using System.Reflection;
using ServerEngine.Core.Enums;
using ServerEngine.IO;

namespace ServerEngine
{
    public abstract class ServerBehavior
    {
        public delegate void EventServer(RemoteClient client);

        public static event EventServer OnDisconnectClient;

        public static event EventServer OnConnectClient;

        public ServerBehavior()
        {
            
        }

        public void AddToPool(object instance)
        {
            Core.Core.AddToPool(instance);
        }

        public static void Core_OnClientDisconnected(RemoteClient client)
        {
            OnDisconnectClient?.Invoke(client);
        }

        public static void Core_OnClientConnected(RemoteClient client)
        {
            OnConnectClient?.Invoke(client);

            ServerBehavior.SendRPC("Init", ConfigController.g_SystemConfig.tickrate);
        }

        /// <summary>
        /// Используеться для вызова rpc команды на стороне клиента.
        /// </summary>
        /// <param name="namemethod">Название метода которое вы хотите вызвать на стороне клиента</param>
        /// <param name="parametrs">Переменные которые нужно передать в метод</param>
        public static void SendRPC(string namemethod, params object[] parametrs)
        {
            string fullparamname = "";

            foreach(var item in parametrs)
            {
                fullparamname += $"{item.GetType().ToString()};";
            }

            byte[] data = Program.corepack.Topack(namemethod, fullparamname, parametrs);

            Program.MultiCast(data);
        }

        public static void SendIgnoreRPC(string namemethod, RemoteClient client, params object[] parametrs)
        {
            string fullparamname = "";

            foreach (var item in parametrs)
            {
                fullparamname += $"{item.GetType().ToString()};";
            }

            byte[] data = Program.corepack.Topack(namemethod, fullparamname, parametrs);

            Program.IgnoreCast(client, data);
        }

        public static void SendSingleRPC(string namemethod, RemoteClient client, params object[] parametrs)
        {
            string fullparamname = "";

            foreach (var item in parametrs)
            {
                fullparamname += $"{item.GetType().ToString()};";
            }

            byte[] data = Program.corepack.Topack(namemethod, fullparamname, parametrs);

            Program.SingleCast(client, data);
        }
    }
}
