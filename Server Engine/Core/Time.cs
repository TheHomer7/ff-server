﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerEngine.Core
{
    public class Time
    {

        public static double time 
        {
            get
            {
                return Core._time;
            }
        }

        public static double deltaTime
        {
            get
            {
                return Core._deltatime;
            }
        }

    }
}
