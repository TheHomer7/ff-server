﻿using ServerEngine;
using ServerEngine.Core.Attributes;
using ServerEngine.Core.Interfaces;
using System;

namespace TestServer
{
    public class RPCTEST : ServerBehavior, ITicked
    {
        public RPCTEST()
        {
            AddToPool(this);
            Console.WriteLine("Pidorok");
        }

        [RPC]
        public void Test(int test, string test2)
        {
            Console.WriteLine($"{test}:poshel nahui {test2}");
        }

        public void Update()
        {
            throw new NotImplementedException();
        }
    }
}
