﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ServerEngine.Core
{
    public class ObjectPool<T>
    {
        private List<T> _objects;

        public ObjectPool()
        {
            _objects = new List<T>();
        }

        public List<T> GetAllObject()
        {
            lock (_objects)
            {
                return _objects;
            }
        }

        public void PutObject(T item)
        {
            lock (_objects)
            {
                _objects.Add(item);
            }
        }

        public void RemoveObject(T item)
        {
            lock (_objects)
            {
                _objects.Remove(item);
            }
           
        }
    }

}
