﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ServerEngine.Core
{
    public class RpcPool
    {
        private Dictionary<string, MethodInfo> _objects;

        public RpcPool()
        {
            _objects = new Dictionary<string, MethodInfo>();
        }

        public void PutObject(string name, MethodInfo methodInfo)
        {
            lock (_objects)
            {
                _objects.Add(name, methodInfo);
            }
        }
        public MethodInfo ExecuteObject(string name)
        {
            lock (_objects)
            {
                return _objects[name];                
            }
        }
        public int Count
        {
            get
            {
                lock (_objects)
                {
                    return _objects.Count;
                }
            }
        }
        public void RemoveObject(string name)
        {
            lock (_objects)
            {
                _objects.Remove(name);
            }

        }
    }
}
