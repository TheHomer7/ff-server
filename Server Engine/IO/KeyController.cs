﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ServerEngine.IO
{
    class KeyController
    {
        private static string NameFolderKeys = "Keys";

        private static string PathBase = AppDomain.CurrentDomain.BaseDirectory;

        private static string KeysFolderPath = Path.Combine(PathBase, NameFolderKeys);

        private static string DefaultExtension = ".key";

        public static string WriteKey(byte[] key, string name)
        {
            if (!Directory.Exists(KeysFolderPath))
                Directory.CreateDirectory(KeysFolderPath);


            if (File.Exists(Path.Combine(KeysFolderPath, name + DefaultExtension)))
            {
                return $"false:Такой файл ключа уже существует {name}";
            }


            string path = Path.Combine(KeysFolderPath, name + DefaultExtension);

            var key_file = File.Create(path);

            using (FileStream fstream = key_file)
            {
                byte[] array = key;
                fstream.Write(array, 0, array.Length);
                return $"true:Файл ключа {name} успешно создан.";
            }


        }

        public static byte[] ReadKey(string name)
        {
            var path = Path.Combine(KeysFolderPath, name + DefaultExtension);

            if (!File.Exists(path))
            {
                return Array.Empty<byte>();
            }

            var file = File.Open(path, FileMode.Open);

            using (FileStream fstream = file)
            {
                byte[] array = new byte[fstream.Length];
                fstream.Read(array, 0, array.Length);
                return array;
            }
        }
    }
}
