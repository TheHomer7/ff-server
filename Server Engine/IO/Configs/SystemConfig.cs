﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerEngine.IO.Configs
{
    public class SystemConfig : IConfig
    {
        public SystemConfig()
        {
            this.ip = "127.0.0.1";
            this.port = 5000;
            this.maxplayer = 1000;
            this.tickrate = 64;
            this.protocolid = 0x190E24L;
        }

        public SystemConfig(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
            this.maxplayer = 1000;
            this.tickrate =  64;
            this.protocolid = 0x190E24L;
        }

        public SystemConfig(string ip, int port, int tps)
        {
            this.ip = ip;
            this.port = port;
            this.maxplayer = 1000;
            this.tickrate = tps;
            this.protocolid = 0x190E24L;
        }

        public SystemConfig(string ip, int port, int tps, int maxplayer)
        {
            this.ip = ip;
            this.port = port;
            this.maxplayer = maxplayer;
            this.tickrate = tps;
            this.protocolid = 0x190E24L;
        }

        public string ip { get; set; }
        public int port { get; set; }
        public int tickrate { get; set; }
        public int maxplayer { get; set; }
        public ulong protocolid { get; set; }
    }
}
