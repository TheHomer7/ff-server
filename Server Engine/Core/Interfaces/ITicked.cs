﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerEngine.Core.Interfaces
{
    public interface ITicked
    {
        public abstract void Update();
    }
}
