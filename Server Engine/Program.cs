﻿using NetcodeIO.NET;
using ServerEngine.Core;
using ServerEngine.Core.Enums;
using ServerEngine.IO;
using ServerEngine.IO.Configs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ServerEngine
{
    public class Program
    {
        public delegate void EventServer();

        public static event EventServer OnStartingServer;

        public static event EventServer OnStartServer;

        public static event EventServer AwakeEvent;
        public static event EventServer StartEvent;

        public static Datapack corepack;

        private static byte[] private_key = new byte[]
        {

           0xf0, 0x0e, 0x31, 0x58, 0x83, 0x41, 0xb8, 0xab,
           0x0b, 0xe6, 0x3d, 0x28, 0x64, 0x1c, 0x2d, 0x06,
           0x4d, 0x15, 0x50, 0x00, 0xa4, 0xcf, 0xa4, 0xb6,
           0x11, 0x51, 0x63, 0x9e, 0x5c, 0x7a, 0x95, 0xf2

        };
        public static Server core;

        static void Main(string[] args)
        {
            BindingEvents.BindEnvents();
            OnStartingServer?.Invoke();

            var config = ConfigController.g_SystemConfig;
            Console.WriteLine($"TickRate Config: {config.tickrate}");
            core = new Server(config.maxplayer, config.tickrate, config.ip, config.port, config.protocolid, private_key);
            
            core.LogLevel = NetcodeLogLevel.Debug;

            Core.Core.logLevel = Core.Enums.LogLevel.Debug;

            Core.Core.Initialize();

            Core.Core.ExecuteAssemblyMain();

            corepack = new Datapack("10000000000000000000", Core.Core.ServerAssembly);

            core.OnClientDisconnected += ServerBehavior.Core_OnClientDisconnected;
            core.OnClientConnected += ServerBehavior.Core_OnClientConnected;
            core.OnClientMessageReceived += Core_OnClientMessageReceived;
            OnStartServer?.Invoke();
            core.Start();

            while (true)
            { 
                Thread.Sleep(1);
            }
        }


        public static void StopServer(string reason)
        {
            core.Stop();
            Console.WriteLine(reason);
        }

        private static void Core_OnClientMessageReceived(RemoteClient sender, byte[] payload, int payloadSize)
        {
            if (Core.Core.logLevel >= LogLevel.DebugCore)
            {
                Console.WriteLine($"Execute RPC {sender.ClientID} datapack size {payloadSize}");
            }

            var data = corepack.Unpack(payload);

            string FullNameRpc = data["name"] + "::" + data["type"];

            Core.Core.ExecuteRPC(FullNameRpc, (data["param"].ToArray()));

        }
        public static void MultiCast(byte[] data)
        {
            Task.Run(() =>
            {
                var clients = Core.Core.remoteClients.Values;

                foreach(var item in clients)
                {
                    core.SendPayload(item, data, data.Length);
                }

            });
        }

        public static void IgnoreCast(RemoteClient ignoreclient, byte[] data)
        {
            Task.Run(() =>
            {
                var clients = Core.Core.remoteClients.Values.Where(x => x.ClientID != ignoreclient.ClientID);

                foreach (var item in clients)
                {
                    core.SendPayload(item, data, data.Length);
                }

            });
        }

        public static void SingleCast(RemoteClient client, byte[] data)
        {
            core.SendPayload(client, data, data.Length);
        }

    }
}

