﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ServerEngine.Core
{
    public class Datapack
    {
        public string uid;
        private Assembly assembly;

        public Datapack(string uid, Assembly assembly)
        {
            this.uid = uid;
            this.assembly = assembly;
        }

        private byte[] ClearNull(byte[] data)
        {
            try
            {
                // populate foo
                int i = data.Length - 1;
                while (data[i] == 0)
                    --i;
                // now foo[i] is the last non-zero byte
                byte[] result = new byte[i + 1];
                Array.Copy(data, result, i + 1);

                return result;
            }
            catch
            {
                return Array.Empty<byte>();
            }
        }

        public byte[] Topack(string name, string types, params dynamic[] parameters)
        {
            byte[] uidsender = Encoding.ASCII.GetBytes(uid);

            byte[] namemethod = Encoding.ASCII.GetBytes(name);

            byte[] typesmethod = Encoding.ASCII.GetBytes(types);

            byte[] pstring = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(parameters));

            int indexes = 12;

            byte[] s_names = BitConverter.GetBytes(indexes + uidsender.Length);

            byte[] s_types = BitConverter.GetBytes(indexes + uidsender.Length + namemethod.Length);

            byte[] s_params = BitConverter.GetBytes(indexes + uidsender.Length + namemethod.Length + typesmethod.Length);

            List<byte> pack = new List<byte>();

            pack.AddRange(s_types);
            pack.AddRange(s_params);
            pack.AddRange(s_names);

            pack.AddRange(uidsender);
            pack.AddRange(namemethod);
            pack.AddRange(typesmethod);
            pack.AddRange(pstring);

            return pack.ToArray();
        }


        public Dictionary<string, dynamic> Unpack(byte[] data)
        {
            string uid;
            string name;
            string type;
            List<dynamic> param = new List<dynamic>();

            byte[] s_types = new byte[4];
            byte[] s_params = new byte[4];
            byte[] s_names = new byte[4];

            Array.Copy(data, s_types, 4);
            Array.Copy(data, 4, s_params, 0, 4);
            Array.Copy(data, 8, s_names, 0, 4);

            int s_type = BitConverter.ToInt32(s_types, 0);

            int s_param = BitConverter.ToInt32(s_params, 0);

            int s_name = BitConverter.ToInt32(s_names, 0);


            int l_uid = s_name - 12;
            byte[] r_uid = new byte[l_uid];
            Array.Copy(data, 12, r_uid, 0, l_uid);
            uid = Encoding.ASCII.GetString(r_uid);

            int l_name = s_type - s_name;
            byte[] r_name = new byte[l_name];
            Array.Copy(data, s_name, r_name, 0, l_name);
            name = Encoding.ASCII.GetString(r_name);

            int l_type = s_param - s_type;
            byte[] r_type = new byte[l_type];
            Array.Copy(data, s_type, r_type, 0, l_type);
            type = Encoding.ASCII.GetString(r_type);

            int l_param = data.Length - s_param;
            byte[] r_param = new byte[l_param];
            Array.Copy(data, s_param, r_param, 0, l_param);
            param = JsonConvert.DeserializeObject<List<dynamic>>(Encoding.ASCII.GetString(r_param));

            //гост 5812-82
            var p_type = type.Split(';');

            var types = new List<Type>();

            foreach (var item in p_type)
            {
                if (String.IsNullOrEmpty(item))
                    continue;

                Type c_type = Type.GetType(item, false);
                if (c_type == null)
                {
                    c_type = assembly.GetType(item, false, true);
                }
                types.Add(c_type);
            }
            List<dynamic> c_param = new List<dynamic>();
            for (int i = 0; i < param.Count; i++)
            {
                var item = param[i];
                var strInput = item.ToString().Trim();
                if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || (strInput.StartsWith("[") && strInput.EndsWith("]")))
                {
                    var convert_item = JsonConvert.DeserializeObject(item.ToString(), types[i]);
                    c_param.Add(convert_item);
                }
                else
                {
                    var convert_item = Convert.ChangeType(item, types[i]);
                    c_param.Add(convert_item);
                }

            }
            //конец

            var result = new Dictionary<string, dynamic>();

            result.Add("uid", uid);
            result.Add("name", name);
            result.Add("type", type);
            result.Add("param", c_param);

            return result;
        }
    }
}
